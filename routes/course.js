const express = require("express");
const router = express.Router();
const courseController = require("../controllers/course")
const auth = require("../auth")

// Create Course

router.post("/", auth.verify, (req,res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin
	console.log(isAdmin)
	if (isAdmin) {
		res.send(`User is admin, course successfully created!`)
		courseController.addCourse(req.body)
	}
	else {
		res.send(`User is not admin, cannot create course.`)
	}
})

// Retrieve All Course

router.get("/all", (req,res) => {
	courseController.getAllCourses().then(resultFromController => res.send(resultFromController))
});

// Retrieve All Active Course

router.get("/", (req, res) => {
	courseController.getAllActive().then(resultFromController => res.send(resultFromController))
});

// Retrieve Specific Course
router.get("/:courseId", (req, res) => {
	console.log(req.params)
	console.log(req.params.courseId)
	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController))
})

//Update a course
router.put("/:courseId", auth.verify, (req,res) => {
	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController))
})

// Archiving a Specific Course
router.put("/:courseId/archive", auth.verify, (req,res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin
	console.log(isAdmin)
	if (isAdmin) {
		courseController.archiveCourse(req.params, req.body).then(resultFromController => (res.send(`${resultFromController}. User is admin, course successfully archived!`)))
		
	}
	else {
		res.send(`User is not admin, cannot archive course.`)
	}
})

module.exports = router;
